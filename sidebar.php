<!-- This sidebar file is intended to hold home/frontpage text boxes and slider -->

<?php	$frontpageBoxes = new WP_Query( 'pagename=frontpage' ); while ( $frontpageBoxes->have_posts() ) : $frontpageBoxes->the_post(); ?>
<?php

		$actionTitle = get_field( "action_title" );
		$actionText = get_field( "action_text" );
		$aboutTitle = get_field( "about_title" );
		$aboutText = get_field( "about_text" );
		$aboutImage = get_field( "about_image" );
		$supportTitle = get_field( "support_title" );
		$supportText = get_field( "support_text" );
?>		
<?php endwhile; wp_reset_postdata(); ?>


<div id="text-areas">
	<div id="upper-container">

		<!-- Upper left box -->
		<div class="action">
			<div class="marker"></div>
			<div class="action-inner">
				<div class="action-header">
					<h3><?php echo $actionTitle; ?></h3>
				</div>
				<div class="action-text-area">
					<?php echo $actionText; ?>	
				</div>
			</div>
		</div>
		<!-- Upper left box ends -->

			<!-- New Products Slider -->
		<div class="new-slider-container">
			<div class="marker"></div>
			<div class="new-inner">
				<div class="new-header">
					<h3>New Products</h3>
					<div class="controls">
						<div class="control-container prevcont">
							<span class="control prev">Prev</span>
						</div>
						<ul class="triggers">
						</ul>
						<div class="control-container nextcont">
							<span class="control next">Next</span>
						</div>
					</div>
				</div>
				<?php $test = get_field( "support_title" ); echo $test; ?>

				<div class="slider-wrapper">
					<div class="sliding-content">
				
						<?php	$nslides = new WP_Query( 'pagename=frontpage' );
				    	while ( $nslides->have_posts() ) : $nslides->the_post(); 
				    		if(get_field('new_products_slider')): 
				     			while(the_repeater_field('new_products_slider')): ?>

		    		<div>
							<h2><?php the_sub_field('slide_title'); ?></h2>
				    	<div class="news-textarea">
				    		<?php the_sub_field('slide_text'); ?>
				    	</div>
				    	<?php if( get_sub_field('learn_more_link') ): ?>
				    	<div class="news-learn-more-link"><a class="slider-link" href="<?php the_sub_field('learn_more_link'); ?>">Learn more</a></div>
				    	<?php endif; ?>
				    	<div class="image-container">
								<?php $image = wp_get_attachment_image_src(get_sub_field('slider_image'), 'medium'); ?>
					    	<img src="<?php echo $image[0]; ?>" />
				    	</div>
			    	</div>

			    	<?php endwhile; ?>
			    		<?php endif; ?>
			    		<?php endwhile; wp_reset_postdata(); ?> 

					</div>	
				</div>


			</div>
		</div>
		<!-- New Products slider ends -->
	
	</div> <!-- Upper container ends -->

	<div id="bottom-container">
		
		<div class="about-us">
			<div class="marker"></div>
			<div class="about-inner">
				<div class="about-image">
					<img src="<?php echo $aboutImage ?>">
				</div>
				<div class="about-text-area">
					<h3><?php echo $aboutTitle; ?></h3>
					<div><?php echo $aboutText; ?></div>
				</div>
			</div>
		</div>

		<div class="service">
			<h3><?php echo $supportTitle; ?></h3>
			<div><?php echo $supportText; ?></div>
			<div class="marker-horizontal"></div>
	</div>

	</div>	

</div> <!-- Text areas container ends -->


