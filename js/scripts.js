jQuery(document).ready(function($){

	/**
		Active menu rollover handeling 
	**/

	$(function () {
	  $(".menu-item").not(".current_page_item").mouseover(function () {
	  	$(this).addClass("with-bg");
			$(".current_page_item .menu-bg ").css('background-image', 'none');
		});
		
		$(".menu-item").mouseleave(function () {
			$(this).removeClass("with-bg");
			$(".current_page_item .menu-bg ").css('background-image', '');
	  });
	});


	/**
		Video resize
	**/

	$(function () {
	  //$("#content").fitVids();
	});


});