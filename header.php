<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>Starter <?php wp_title('/', true,''); ?> </title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/glyphicons.css" type="text/css" media="screen" />
	<meta name="viewport" content="width=device-width" />
	<meta name="keywords" content="">
	<meta name="description" content="">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

		<header>
			<div class="container">
				<div id="logo-container">
					<a id="logo" href="<?php bloginfo('home'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/logo.gif"></a>
				</div>	
				<?php //get_search_form(); ?>
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'link_after' => '<span class="glyphicon glyphicon-triangle-right"></span>') ); ?>
			</div>
		</header>




