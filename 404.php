<?php get_header(); ?>

	<div id="primary" class="content-area">
			<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.'); ?></h1>
	</div><!-- //content-area -->

<?php get_footer(); ?>
