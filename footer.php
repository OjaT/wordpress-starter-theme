<footer>
	<div class="container">
		<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
