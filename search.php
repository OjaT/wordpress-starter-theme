<?php get_header(); ?>

	<section id="primary" class="content-area">
		<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
		<?php while ( have_posts() ) : the_post(); ?>
			<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
			<?php the_content(); ?>
		<?php endwhile; ?>

	</section><!-- .content-area -->

<?php get_footer(); ?>
